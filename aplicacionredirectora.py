# EJERCICIO 17.4. APLICACIÓN REDIRECTORA

import socket
import random


port = 1234
mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
mySocket.bind(('localhost', port))


mySocket.listen(5)


while True:
    print("Waiting for connections")
    (recvSocket, address) = mySocket.accept()
    print("HTTP request received:")
    received = recvSocket.recv(2048)
    print(received)
    paginas = ['https://es.wikipedia.org', 'http://localhost:1234/', 'https://google.es',
                     'https://www.aulavirtual.urjc.es', 'https://labs.etsit.urjc.es/']

    response = (b"HTTP/1.1 301 Moved Permanently\r\n\r\n" +
                b"<html><body><h1>Bienvenido!</h1>" + b"<h3>Redirigiendo...Espere un momento</h3>" +
                b"<meta http-equiv='refresh' content='3; url=" + bytes(random.choice(paginas), 'utf-8') + b"'>" +
                b"</body></html>" + b"\r\n")
    recvSocket.send(response)
    recvSocket.close()

